\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{notes}[Notes]

\RequirePackage[german]{babel}
\LoadClass[a4paper, 12pt]{book}
\RequirePackage{csquotes}
\RequirePackage{amsthm}
\RequirePackage[backend=bibtex,bibstyle=authoryear]{biblatex}
\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{tikz}
\RequirePackage{libertine}
\RequirePackage{hyperref}
\RequirePackage{ulem}
\RequirePackage{calc}

\theoremstyle{definition}
\newtheorem{definition}{Def}[section]
\newtheorem{theorem}{Satz}[section]

\theoremstyle{remark}
\newtheorem*{remark}{Bem}
\newtheorem*{addition}{Zusatz zur Def}
\newtheorem*{bez}{Bezeichnung}

\setlength{\parindent}{0pt}

\newcommand*{\estimate}{\mathrel{\hat=}}

\newcommand\bracetext[2]{%
  \par\smallskip
   \noindent\makebox[\textwidth][r]{$\text{#1}\left\{
    \begin{minipage}{\textwidth}
    #2
    \end{minipage}
  \right.\nulldelimiterspace=0pt$}\par\smallskip
}   

\newcommand{\notestitle}[2]{
  \begin{titlepage}
	\centering
	{\scshape\LARGE Heinrich Heine Universität Düsseldorf\par}
	\vspace{1cm}
	{\scshape\Large Digitalisiert von Studenten für Stundenten\par}
	\vspace{1.5cm}
	{\huge\bfseries #1\par}
	\vspace{2cm}
	{\Large\itshape Skript von #2\par}
	\vfill
    % Bottom of the page
	{\large \today\par}
  \end{titlepage}
}
