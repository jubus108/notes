\chapter{Grundlagen}

\section{Mengen und Abbildungen}

Von fundamentaler Bedeutung sowohl für die Analysis wie auch für die Algebra ist der Begriff der Menge. Er wurde 1895 von Georg Cantor folgendermaßen eingeführt:

\begin{definition}{(Menge, Element)}
  ``Unter einer Menge verstehen wir jede Zusammenfassung $M$ von bestimmten wohlunterschiedenen Objekten in unserer Anschauung oder unseres Denkens(welche die Elemente von $M$ genannt werden) zu einem Ganzen.'' \cite{Cantor:1895}
\end{definition}

Dies ist keine exakte Definition, denn sie enthält Begriffe wie ``Zusammenfassung'' und ``Objekt'', die nicht genauer festgelegt sind. Ferner führt sie zu Widersprüchen, wie wir bald anhand eines einfachen Beispiels sehen werden. Um zumindest die größten Schwierigkeiten aus dem Weg zu räumen, fügen wir hinzu:

\begin{addition}
  Hierbei muß prinzipiell entscheidbar sein, ob ein Element zu einer Menge $M$ gehört oder nicht.
\end{addition}

\begin{bez}
  Wir schreiben $m \in M$ (gelesen: ``$m$ Element $M$''), wenn das Objekt $m$ zur Menge $M$ gehört, anderenfalls $m \notin M$.
\end{bez}

Es gibt zwei Möglichkeiten der Beschreibung von Mengen:

\paragraph{1. Durch Aufzählung}

\[
  M_1 = \{ 2, 4, 6, 8 \} ~~ oder ~~ M_2 = \{ 7, X, \Delta, ! \}
\]

es können also durchaus verschiedenartige Objekte zu einer Menge zusammen\-ge\-fasst werden. Auf die Reihenfolge kommt es hierbei nicht an und in der Regel wird jedes Objekt nur einmal genannt (beachte: ``wohlunterschiedlich'' in Cantors Definition!). Wir haben also

\[
  M_1 = \{ 4, 8, 6, 2 \} = \{ 2, 4, 2, 6, 8 \}
\]

Diese Art der Beschreibung ist in erster Linie geeignet für endliche Mengen, das sind Mengen mit endlich vielen Elementen. Aber auch

\[
  M_3 = \{ 2, 4, 6, 8, \dots \}
\]

ist eine zulässige Beschreibung der Menge aller geraden natürlichen Zahlen. Es kommt dabei darauf an, dass ein Bildungsgesetz eindeutig erkennbar ist.

\paragraph{2. Durch Angabe einer chakterisierenden Eigenschaft E(x)}

\[
  \text{allgemein in der Form}~M = \{ x : E(x) \},
\]

was man lesen würde als ``Menge aller Objekte x mit der Eigenschaft E''. Z.B. haben wir

\[
  M_1 = \{ x : \text{$x$ ist eine gerade natürliche Zahl, die kleiner ist als 10} \}
\]

oder

\[
  M_3 = \{ x : \text{$x$ ist eine gerade natürliche Zahl} \}.
\]

\emph{Vorsicht!} Nicht jede Angabe einer charakterisierenden Eigenschaft führt zu einer wohldefinierten Menge.

Bsp. (``Russel'sche Unmenge''): Die Menge

\[
  M_R := \{ x: \text{$x$ ist Menge und $x \notin x$} \}
\]

aller Mengen, die sich selbst als Element nicht enthalten ist zweifellos eine ``Zusammenfassung von Objekten unseres Denkens'', genügt also der Cantor\-'schen Mengen\-definition. Aber: Gehört $M_R$ als Element zu $M_R$, d.h. Gilt $M_R \in M_R$?

Nehmen wir dies an, folgt sofort das Gegenteil:

\[
  M_R \in M_R = \{ x : x \notin x \} \Rightarrow M_R \notin M_R
\]

\[
  \text{Umgekehrt:}~ M_R \notin M_R = \{ x: x \notin x \} \Rightarrow M_R \in M_R.
\]

Die Frage ``$M_R \in M_R$?'' ist also \emph{nicht entscheidbar}.

Dies motiviert den oben formulierten Zusatz zur Cantor'schen Definition.

Hingegen ist

\[
  M_4 = \{ x : \text{$x$ ist eine Primzahl} \}
\]

eine wohldefinierte Menge, auch wenn heute (noch) niemand entscheiden kann, ob z.B. $2^{99991}-1$ eine Primzahl ist oder nicht. Es ist prinzipiell entscheidbar, $M_4$ genügt damit dem Zusatz zur Definition. Die Menge aller Primzahlen ist wohldefiniert.

Das Beispiel der Russel'schen Unmenge zeigt: Probleme mit dem sogenannten ``naiven'' Cantor'schen Mengenbegriff treten dann auf, wenn man Mengen von Mengen untersucht. Hat man es mit Mengen gleichartiger Objekte (z.B. mit Zahlenmengen) zu tun, so treten derartige Schwierigkeiten nicht auf.

Beispiele von Zahlenmengen, die in der Analysis \RN{1} von Bedeutung sind:

\begin{itemize}
\item $\mathbb{N}= \{1,2,3,\dots\}$ \hfill (natürliche Zahlen)
\item $\mathbb{N}_0= \{0,1,2,\dots\}$ \hfill (natürliche Zahlen mit 0)
\item $\mathbb{Z} = \{0, \pm 1, \pm 2, \dots\}$ \hfill (ganze Zahlen)
\item $\mathbb{Q} = \{\frac{p}{q} : p \in \mathbb{Z}, q \in \mathbb{N} \}$ \hfill (rationale Zahlen)
\item $\mathbb{R} = \{a + \frac{a_{1}}{10^{1}} + \frac{a_{2}}{10^{2}} + \dots : a \in \mathbb{Z}, a_1, a_2,\dots \in \{0,\dots,9 \} \}$ \hfill (reelle Zahlen)
\end{itemize}

Intervalle: Für $a,b\in\mathbb{R}$ mit $a<b$ definiert man

\vspace{1em}\hspace{1em} $[a,b] := \{x \in \mathbb{R} : a \leq x \leq b\}$ \hfill (abgeschlossen)

\vspace{1em}\hspace{1em} $(a,b) = ~]a,b[~ := \{x \in \mathbb{R} : a < x < b \}$ \hfill (offen)

\vspace{1em}\hspace{1em} $\left.
\begin{tabular}{@{}c@{}}
\( [a,b) := \{ x \in \mathbb{R} : a \leq x < b \} \) \\
\( (a,b] := \{ x \in \mathbb{R} : a < x \leq b \} \)
\end{tabular}
\hspace{0.5em}\right\}$
\hfill (halboffen)

\vspace{1em}sowie uneigentliche ($\estimate$ ``unendlich ausgedehnte'') Intervalle wie z.B.

\vspace{1em}\hspace{1em} $[a,\infty) := \{ x \in \mathbb{R} : x \geq a \}$\hspace{1em}oder

\vspace{1em}\hspace{1em} $(-\infty,b) := \{ x \in \mathbb{R} : x < b \}$

\begin{remark}\hfill
  \begin{enumerate}
  \item[$i$.] Das Symbol $\infty$ für ``unendlich'' ist \emph{keine reelle Zahl}! Die Rechenregeln für reelle Zahlen sind für dieses Symbol \emph{nicht} anwendbar!
  \item[$ii$.] Die Ordnungsrelationen $<$ und $\leq$ zwischen zwei reellen Zahlen werden wir später genauer fassen.
  \end{enumerate}
\end{remark}

Bei dieser Auszählung von Zahlenmengen, insbesondere bei den Intervallen haben wir in gewisser Weise den folgenden Begriff der Teilmenge vorweggenommen:

\begin{definition}{Teilmenge, Mengengleichheit und leere Menge}
  \begin{enumerate}
  \item[($i$)] Eine Menge $M_1$ heißt Teilmenge einer Menge $M_2$, falls alle Elemente von $M_1$ in $M_2$ enthalten sind.

    \[
      \text{Schreibweise: } M_1\subset M_2,~\text{falls gilt: } x \in M_1 \Rightarrow x \in M_2
    \]
  \item[($ii$)] Zwei Mengen $M_1$ und $M_2$ heißen gleich, wenn sie die gleichen Elemente enthalten, Kurz:

    \[
      M_1 = M_2 :\iff M_1 \subset M_2~\text{und}~M_2 \subset M_1
    \]
    
  \item[($iii$)] Die leere Menge ist diejenige Menge, welche kein Element enthält. Sie wird mit $\emptyset$ (oder $\{~\}$) bezeichnet.
  \end{enumerate}
\end{definition}

\begin{remark}
  Für jede Menge $M$ gilt $\emptyset \subset M$.
\end{remark}

\begin{definition}{Mengenverknüpfungen}
  $M_1$ und $M_2$ seien Mengen.

  Dann heißen:

  \begin{enumerate}
  \item[($i$)] $M_1 \cup M_2 := \{ x : x \in M_1 \text{ oder } x \in M_2 \}$ die \emph{Vereinigung}
  \item[($ii$)] $M_1 \cap M_2 := \{ x : x \in M_1 \text{ und } x \in M_2 \}$ der \emph{(Durch-)Schnitt}
  \item[($iii$)] $M_1 \setminus M_2 := \{ x : x \in M_1 \text{ und } x \notin M_2 \}$ die \emph{Differenz}
  \item[($iv$)] $M_1 \bigtriangleup M_2 := (M_1 \setminus M_2) \cup (M_2 \setminus M_1)$ die \emph{symmetrische Differenz}
  \end{enumerate}

  der Mengen $M_1$ und $M_2$.
\end{definition}

\begin{remark}
  Ohne den Begriff der leeren Menge hätten wir den Durchschnitt $M_1 \cap M_2$ nicht für beliebige Mengen $M_1$ und $M_2$ definieren können. Zwei Mengen mit der Eigenschaft $M_1 \cap M_2 = \emptyset$ nennt man \emph{disjunkt}.
\end{remark}

Diese Mengenoperationen können durch sogenannte \emph{Venn-Diagramme} veranschaulicht werden:

\begin{center}
  \begin{tikzpicture}
    \fill[darkgray] (3.0, 1.0) circle [radius=0.75];
    \fill[darkgray] (4.0, 1.0) circle [radius=0.75];
    \draw[black] (3.0, 1.0) circle [radius=0.75];
    \draw[black] (4.0, 1.0) circle [radius=0.75];
    \node at (0.2, 1.0) {($i$) $M_1 \cup M_2$};
  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}
    \begin{scope}
      \clip (3.0, 1.0) circle [radius=0.75];
      \fill[darkgray] (4.0, 1.0) circle [radius=0.75];
    \end{scope}
    \draw[black] (3.0, 1.0) circle [radius=0.75];
    \draw[black] (4.0, 1.0) circle [radius=0.75];
    \node at (0.2, 1.0) {($ii$) $M_1 \cap M_2$};
  \end{tikzpicture}
  \vskip 2em
  \begin{tikzpicture}
    \begin{scope}
      \clip (3.0, 1.0) circle [radius=0.75];
      \fill[darkgray, even odd rule] (3.0, 1.0) circle [radius=0.75]
                                     (4.0, 1.0) circle [radius=0.75];
    \end{scope}
    \draw[black] (3.0, 1.0) circle [radius=0.75];
    \draw[black] (4.0, 1.0) circle [radius=0.75];
    \node at (0.2, 1.0) {($iii$) $M_1 \setminus M_2$};
  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}
    \fill[darkgray, even odd rule] (3.0, 1.0) circle [radius=0.75]
                                   (4.0, 1.0) circle [radius=0.75];
    \draw[black] (3.0, 1.0) circle [radius=0.75];
    \draw[black] (4.0, 1.0) circle [radius=0.75];
    \node at (0.2, 1.0) {($iv$) $M_1 \bigtriangleup M_2$};
  \end{tikzpicture}
\end{center}

Solche Diagramme sind oft nützlich, haben aber keine Beweiskraft.

\begin{theorem}{Rechenregeln für Mengenverknüpfungen}
  $M_1$, $M_2$ und $M_3$ seien Mengen. Dann gelten:

  \begin{enumerate}
  \item[($i$)] $M_1 \cup M_2 = M_2 \cup M_1;~ M_1 \cap M_2 = M_2 \cap M_1$ \hfill (Kommutativität) 
  \item[($ii$)] $M_1 \cup (M_2 \cup M_3) = (M_1 \cup M_2) \cup M_3$, desgl. für $\cap$ \hfill (Assoziativität)
  \item[($iii$)] $\left.
    \begin{tabular}{@{}c@{}}
    \( M_1 \cap ( M_2 \cup M_3 ) = (M_1 \cap M_2) \cup (M_1 \cap M_3) \) \\
    \( M_1 \cup (M_2 \cap M_3) = (M_1 \cup M_2) \cap (M_1 \cup M_3) \)
    \end{tabular}
    \hspace{0.5em}\right\}$
    \hfill (Distributivität)

  \end{enumerate}
\end{theorem}

Exemplarisch soll der Beweis des ersten Distributivgesetzes durchgeführt werden:

\begin{proof}
\begin{align*}
  x \in M_{1} \cap ( M_{2} \cup M_{3} ) & \Leftrightarrow x \in M_{1} ~und~ x \in M_{2} \cup M_{3} \\
  & \Leftrightarrow x \in M_{1} ~und~ ( x \in M_{2} ~oder~ x \in M_{3} ) \\
  & \Leftrightarrow (x \in M_{1} ~und~ x \in M_{2}) ~oder~ ( x \in M_{1} ~und~ x \in M_{3} ) \\
  & \Leftrightarrow x \in M_{1} \cap M_{2} ~oder~ x \in M_{1} \cap M_{3} \\
  & \Leftrightarrow x \in (M_{1} \cap M_{2}) \cup (M_{1} \cap M_{3})
\end{align*}

Also enthalten $M_{1} \cap (M_{2} \cup M_{3})$ und $(M_{1} \cap M_{2}) \cup (M_{1} \cap M_{3})$ dieselben Elemente und sind daher gleich.
\end{proof}

Neben Mengen von Zahlen bzw. allgemeiner Mengen gleichartiger Objekte aus einer wohldefinierten Grundmenge werden wir aber bereits in der Analysis I Mengen von Mengen betrachten.
